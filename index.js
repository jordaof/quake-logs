var fs = require('fs'),
    QuakeLogParser = require('./app/parser');

fs.readFile('./logs/games.log', 'utf8', function (err,data) {
  if (err) {
    return console.log(err);
  }



  fs.writeFile("gamesLog.json", JSON.stringify(QuakeLogParser(data),undefined,1), "utf8", function(err) {
      if(err) {
          return console.log(err);
      }

      console.log("The file was saved!");
  });

});
