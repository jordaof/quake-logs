function buildPlayerObj(logLine, playersObj) {
  if (logLine.indexOf('ClientUserinfoChanged') !== -1) {
    player = logLine
              .slice(logLine.lastIndexOf(':')+2, logLine.indexOf('\\t'))
              .split(' n\\');
    if(player[0] in playersObj){
    }else{
      playersObj[player[0]] = player[1];
    }
  }
}

function filterKills(logLine, killLog) {
  if (logLine.indexOf('Kill:') !== -1) {
    logLine = logLine
                  .slice(logLine.indexOf('Kill: ')+6, logLine.lastIndexOf(':'))
                  .split(' ');
    killLog.push(logLine);
    }
}

function initScore(players, playersKills) {
  for (var j = 0; j < players.length; j++) {
    pname = players[j];
    playersKills[pname] = 0;
  }
  return playersKills
}

function calcScore(killLog, mods, playersObj, playersKills, killMods) {
  for (var j = 0; j < killLog.length; j++) {
    var killMod = mods[killLog[j][2]];
    if(killLog[j][0] == 1022){
      var killed = playersObj[killLog[j][1]];
      playersKills[killed]--;
    }else{
      var killer = playersObj[killLog[j][0]];
      playersKills[killer]++;
    }

    killMods[killMod] = (killMods[killMod] || 0) +1;
  }
}

function QuakeLogParser(data) {
  var games = [],
      mods = require('./mods'),
      g = 1,
      range = [],
      gamesLog = data.split('\n');

  for (var i = 0; i < gamesLog.length; i++) {
    // split games
    if(gamesLog[i].indexOf('InitGame') !== -1){

      // push index of InitGame to range & check game range(lines)
      range.push(i);
      if(range.length > 1){

        // reset variables
        var obj = {},
            killLog = [],
            players = [],
            playersObj = {},
            killMods = {},
            playersKills = {};

        // increase game number
        name = 'game_'+g.toString();
        matchLog = gamesLog.slice(range[0], range[1]);



        for (var j = 0; j < matchLog.length; j++) {
          // build players Object reference
          buildPlayerObj(matchLog[j], playersObj);


          // Filter kills
          filterKills(matchLog[j], killLog);
        }

        // create players array
        for(player in playersObj){
          players.push(playersObj[player]);
        }

        // populate score with players of the match
        initScore(players, playersKills);

        // calculate game score
        calcScore(killLog, mods, playersObj, playersKills, killMods);

        // Set game object
        obj[name] = {};
        obj[name]['total_kills'] = killLog.length;
        obj[name]['players'] = players;
        obj[name]['kills'] = playersKills;
        obj[name]['killMods'] = killMods;
        games.push(obj);
        // increase game number & discard first element of range
        g++;
        range.shift();
      }
    }
  }
  return games;
}

module.exports = QuakeLogParser;
